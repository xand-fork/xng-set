# XNG-Set

This project contains a script which runs `xand_network_generator` for the specific versions of inputs listed in `.env` files.  It also publishes itself as a versioned Docker container to be used by consumers.

This project is part of a proposal found here: https://miro.com/app/board/o9J_l8VCQ8A=/  Feedback should be given there.

## Project License

MIT OR Apache-2.0

## Why not just package `generated`?

The main reason to package a script that runs XNG instead of XNG output is so XNG for certain version sets can be run with different configured settings i.e. `confidential mode` vs not.

# Versioning

Versioning of this project is done via the file `VERSION.txt` using semver.

# CI / CD Artifacts

Master branch automatically publishes:

- `:$VERSION` tag for the docker image listed in the Makefile
- `:latest` tag for the docker image listed in the Makefile

All Development branches have optional jobs for publishing:

- `:$VERSION-beta-$CI_PIPELINE_IID` tag for the docker image listed in the Makefile

# Using the Published Image

To access the `generated` folder that the script in the docker image produces on the host machine, you can copy that directory to your local machine using `docker cp` after running the image:

```bash
docker run --name xng-set gcr.io/xand-dev/xng-set:latest
docker cp xng-set:/network/generated/ /absolute/path/dest/
docker rm -f xng-set
```

Then you will be able to access `/absolute/path/generated` and run `docker-compose up -d` to launch the generated network on your host machine.

# Using the Makefile
## Build docker image
```bash
make build TARGET=(STABLE or custom build tag target) ARTIFACT_USER=(artifactory_user) ARTIFACT_PASS=(artifactory_pass)
```

## Run Docker Image

```bash
make run TAG=(tag)
```

Note: If you want to override the defaults in `.defaults.env` you'll need to run the image yourself instead of using the makefile and pass in the environment variables you want to override.  e.g.:

```bash
docker run --name xng-set -e ENABLE_AUTH="true" ${image name} 
```

## Kill Running Docker Image

```bash
make kill
```

## Publish Docker Image

```bash
make publish TARGET=(STABLE or custom publish tag target)
```

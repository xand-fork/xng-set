SHELL := /bin/bash

#
#	DOCKER IMAGE HANDLING
#

VERSION_FILE=VERSION.txt

# Image / Container Data
NAME := xng-set
CONTAINER_NAME := $(NAME)
FULL_NAME := gcr.io/xand-dev/$(NAME)
VERSION := $(shell cat $(VERSION_FILE))

# Possible Image:Tag combinations
STABLE_TAG := $(FULL_NAME):$(VERSION)
LATEST_TAG := $(FULL_NAME):latest
CUSTOM_TAG_PREPEND := $(FULL_NAME)

# TARGET
# Variable used in Docker build and publish commands to determine what name:tag combo/s to operate on
# Valid values:
# 	STABLE 			- Uses FULL_NAME:VERSION and FULL_NAME:latest
#	(Value) 		- Uses FULL_NAME:Value
TARGET_STABLE := STABLE

build:
	@if [[ -z "${TARGET}" ]]; then \
		echo 'Error: TARGET var not defined.  Define TARGET= as one of STABLE or a custom tag.'; \
		exit 1; \
	elif [[ "${TARGET}" = "${TARGET_STABLE}" ]]; then \
		./build.sh ${STABLE_TAG} ${LATEST_TAG}; \
	else \
		./build.sh ${CUSTOM_TAG_PREPEND}:${TARGET}; \
	fi

run: kill
	@if [[ -z "${TAG}" ]]; then \
		echo 'Error: TAG var not defined.  Define TAG= as the tag of the ${FULL_NAME} docker image to run.'; \
		exit 1; \
	fi

	@echo 'Running ${TAG} in container ${CONTAINER_NAME}'; \
	docker run --name ${CONTAINER_NAME} ${FULL_NAME}:$(TAG)

kill:
	docker rm -f ${CONTAINER_NAME}

publish:
	@if [[ -z "${TARGET}" ]]; then \
		echo 'Error: TARGET var not defined.  Define TARGET= as one of STABLE or a custom tag.'; \
		exit 1; \
	elif [[ "${TARGET}" = "${TARGET_STABLE}" ]]; then \
		echo "Publishing ${STABLE_TAG} and ${LATEST_TAG}"; \
		docker push ${STABLE_TAG}; \
		docker push ${LATEST_TAG}; \
	else \
		echo "Publishing ${CUSTOM_TAG_PREPEND}:${TARGET}"; \
		docker push ${CUSTOM_TAG_PREPEND}:${TARGET}; \
	fi

